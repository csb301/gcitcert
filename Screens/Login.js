import { View, Text, StyleSheet, Button, TextInput, Image } from "react-native";
import Colors from "../Constants/Color";
import { useEffect, useState } from "react";
import { ethers } from "ethers";
import { useDispatch } from "react-redux";
import { setAccountAddress, setPrivateKey } from "../Store/metaMaskWalletSlice";



function Login({navigation}) {
  const dispatch = useDispatch();

  const [address, setAddress] = useState(""); // store account address from user
  const [key, setKey] = useState(""); // store private key from user

  function handleConnectMetaMask(){
    const wallet = new ethers.Wallet(key);
    if(wallet.address === ethers.utils.getAddress(address)){
      dispatch(setAccountAddress(ethers.utils.getAddress(address)));
      dispatch(setPrivateKey(key));
      setAddress("");
      setKey("");
      navigation.replace("LoginNavigator");
    }else{
      alert("Not able to connet to metamask account");
      setAddress("");
      setKey("");
    }
  }

  // function handleConnectMetamask(){
  //   navigation.replace("LoginNavigator")
  // }
  return (
    <View style={styles.rootContainer}>
      <View style={styles.imageContainer}>
        <Image
          source={require("../assets/gcit_logo.png")}
          style={styles.image}
        />
      </View>


      <Text style={styles.label}>Metamask Wallet Account Address</Text>
      <TextInput
        multiline={true}
        style={styles.textInput}
        placeholder="Enter account address"
        onChangeText={(enteredText) => setAddress(enteredText)} value={address}
      />
      <Text style={styles.label}>Metamask Wallet Private Key</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Enter wallet private key"
        secureTextEntry={true}
        onChangeText={(enteredText => setKey(enteredText))} value={key}
      />
      <View style={styles.button}>
        <Button title="Connect Metamask" onPress={handleConnectMetaMask} color={Colors.secondary} />
      </View>
      <View style={styles.warningContainer}>
        <Text style={styles.warningText}>
          Disclaimer: We do not store your private key anywhere. We just use it
          to validate your account and sign the transaction done by you. Use the
          private key of your wallet cautiously. Do not enter to any app which
          you don't trust.
        </Text>
      </View>
    </View>
  );
}


export default Login;


const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: "center",
  },
  textInput: {
    backgroundColor: Colors.backgroundWhite,
    marginHorizontal: "5%",
    padding: 10,
    marginBottom: 10,
  },
  label: {
    fontSize: 16,
    textAlign: "center",
    paddingBottom: 10,
    fontWeight: "bold",
  },
  button: {
    flexWrap: "wrap",
    alignSelf: "center",
    paddingTop: 10,


},
imageContainer: {
  height: 150,
  width: 150,
  alignSelf: "center",
  borderRadius: 75,
  overflow: "hidden",
  marginBottom: 20,
},
image: {
  width: "100%",
  height: "100%",
},
warningContainer: {
  backgroundColor: Colors.backgroundGrey,
  paddingHorizontal: 10,
  paddingVertical: 5,
  marginHorizontal: "5%",
  marginTop: 15,
  borderTopRightRadius: 10,
  borderTopLeftRadius: 10,
},
warningText: {
  fontStyle: "italic",
  color: "red",
},
});

