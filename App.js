import "@ethersproject/shims";
import "react-native-get-random-values";

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, SafeAreaView} from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import MainNavigator from "./Navigators/MainNavigator";
import Colors from './Constants/Color';
import { Provider } from "react-redux";
import { store } from './Store/store';




export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style='dark' backgroundColor={Colors.secondary}/>
      <Provider store={store}>
        <NavigationContainer>
          <MainNavigator/>
        </NavigationContainer>
      </Provider>
    </SafeAreaView>
  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#D2EFFF',
    backgroundColor: Colors.secondary,
  
  },
  text: {
    fontSize: 25,
    fontWeight: '500',
  },
secondary:{
    primary: "#FFCDB2",
    primary2: "#FFDFC9",
    secondary: "#FFB4A2",
    secondary2: "#FFCEBD",
    background: "#D2EFFF",
    backgroundWhite: "white",
    backgroundGrey: "#ffffff",
    text: "#333333",
  }


});
